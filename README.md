# StrawberryFields Play

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/acefed/strawberryfields-play)

[https://acefed.gitlab.io/strawberryfields](https://acefed.gitlab.io/strawberryfields)

```shell
$ command -v bash curl docker git openssl sed
$ command -v bash curl docker git openssl sed | wc -l
6
```

```shell
$ curl -fsSL https://gitlab.com/acefed/strawberryfields-play/-/raw/main/install.sh | PORT=8080 bash -s -- https://www.example.com
$ curl https://www.example.com/
StrawberryFields Play
```

[https://acefed.gitlab.io/strawberryfields/strawberryfields-play.spdx](https://acefed.gitlab.io/strawberryfields/strawberryfields-play.spdx)  
[https://acefed.gitlab.io/strawberryfields/strawberryfields-play.json](https://acefed.gitlab.io/strawberryfields/strawberryfields-play.json)

SPDX-License-Identifier: MIT
