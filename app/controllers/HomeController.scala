package controllers

import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse
import java.nio.file.Files
import java.nio.file.Paths
import java.security.KeyFactory
import java.security.MessageDigest
import java.security.PublicKey
import java.security.SecureRandom
import java.security.Signature
import java.security.interfaces.RSAPrivateCrtKey
import java.security.spec.PKCS8EncodedKeySpec
import java.security.spec.RSAPublicKeySpec
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import java.util.Locale
import java.util.Base64

import javax.inject._
import play.api._
import play.api.libs.json._
import play.api.mvc._
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import io.github.cdimascio.dotenv.Dotenv

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class HomeController @Inject()(val controllerComponents: ControllerComponents) extends BaseController {

  val dotenv = Dotenv.configure.ignoreIfMissing.load
  val objectMapper = new ObjectMapper
  objectMapper.registerModule(DefaultScalaModule)
  val configJson =
    if (!(dotenv.get("CONFIG_JSON") == null || dotenv.get("CONFIG_JSON").isEmpty)) {
      var result = dotenv.get("CONFIG_JSON")
      if (result.startsWith("'")) result = result.substring(1)
      if (result.endsWith("'")) result = result.substring(0, result.length - 1)
      result
    } else Files.readString(Paths.get("data/config.json"))
  val CONFIG = objectMapper.readTree(configJson)
  val ME = List(
    "<a href=\"https://",
    new URI(CONFIG.get("actor").get(0).get("me").textValue).getHost,
    "/\" rel=\"me nofollow noopener noreferrer\" target=\"_blank\">",
    "https://",
    new URI(CONFIG.get("actor").get(0).get("me").textValue).getHost,
    "/",
    "</a>"
  ).mkString
  val PRIVATE_KEY = importPrivateKey(dotenv.get("PRIVATE_KEY"))
  val PUBLIC_KEY = privateKeyToPublicKey(PRIVATE_KEY)
  val publicKeyPem = exportPublicKey(PUBLIC_KEY)

  def importPrivateKey(pem: String): RSAPrivateCrtKey = {
    val header = "-----BEGIN PRIVATE KEY-----"
    val footer = "-----END PRIVATE KEY-----"
    var b64 = pem
    b64 = b64.replace("\\n", "")
    b64 = b64.replace("\n", "")
    if (b64.startsWith("\"")) b64 = b64.substring(1)
    if (b64.endsWith("\"")) b64 = b64.substring(0, b64.length - 1)
    if (b64.startsWith(header)) b64 = b64.substring(header.length)
    if (b64.endsWith(footer)) b64 = b64.substring(0, b64.length - footer.length)
    val kf = KeyFactory.getInstance("RSA")
    val der = Base64.getDecoder.decode(b64)
    val spec = new PKCS8EncodedKeySpec(der)
    val key = kf.generatePrivate(spec).asInstanceOf[RSAPrivateCrtKey]
    key
  }

  def privateKeyToPublicKey(privateKey: RSAPrivateCrtKey): PublicKey = {
    val kf = KeyFactory.getInstance("RSA")
    val spec = new RSAPublicKeySpec(privateKey.getModulus, privateKey.getPublicExponent)
    val publicKey = kf.generatePublic(spec)
    publicKey
  }

  def exportPublicKey(key: PublicKey): String = {
    val der = key.getEncoded
    var b64 = Base64.getEncoder.encodeToString(der)
    var pem = "-----BEGIN PUBLIC KEY-----" + "\n"
    while (b64.length > 64) {
      pem += b64.substring(0, 64) + "\n"
      b64 = b64.substring(64)
    }
    pem += b64.substring(0, b64.length) + "\n"
    pem += "-----END PUBLIC KEY-----" + "\n"
    pem
  }

  def uuidv7: String = {
    val rand = new SecureRandom
    val v = new Array[Byte](16)
    rand.nextBytes(v)
    val ts = System.currentTimeMillis
    v(0) = (ts >> 40).toByte
    v(1) = (ts >> 32).toByte
    v(2) = (ts >> 24).toByte
    v(3) = (ts >> 16).toByte
    v(4) = (ts >> 8).toByte
    v(5) = ts.toByte
    v(6) = (v(6) & 0x0F | 0x70).toByte
    v(8) = (v(8) & 0x3F | 0x80).toByte
    val sb = new StringBuilder(32)
    for (b <- v) sb.append(String.format("%02x", b))
    sb.toString
  }

  def talkScript(req: String): String = {
    val ts = System.currentTimeMillis
    if (new URI(req).getHost == "localhost") s"<p>${ts}</p>"
    else List(
      "<p>",
      "<a href=\"https://",
      new URI(req).getHost,
      "/\" rel=\"nofollow noopener noreferrer\" target=\"_blank\">",
      new URI(req).getHost,
      "</a>",
      "</p>"
    ).mkString
  }

  def getActivity(username: String, hostname: String, req: String): String = {
    val t = DateTimeFormatter.ofPattern("EEE, dd MMM yyyy HH:mm:ss z", Locale.ENGLISH)
      .withZone(ZoneId.of("GMT"))
      .format(Instant.now)
    val sig = Signature.getInstance("SHA256withRSA")
    sig.initSign(PRIVATE_KEY)
    sig.update(List(
      s"(request-target): get ${new URI(req).getPath}",
      s"host: ${new URI(req).getHost}",
      s"date: ${t}"
    ).mkString("\n").getBytes)
    val b64 = Base64.getEncoder.encodeToString(sig.sign)
    val headers = Map(
      "Date" -> t,
      "Signature" -> List(
        s"keyId=\"https://${hostname}/u/${username}#Key\"",
        "algorithm=\"rsa-sha256\"",
        "headers=\"(request-target) host date\"",
        s"signature=\"${b64}\""
      ).mkString(","),
      "Accept" -> "application/activity+json",
      "Accept-Encoding" -> "identity",
      "Cache-Control" -> "no-cache",
      "User-Agent" -> s"StrawberryFields-Play/2.0.0 (+https://${hostname}/)"
    )
    val builder = HttpRequest.newBuilder
      .uri(new URI(req))
      .GET
    headers.foreach {
      case (k, v) => builder.header(k, v)
    }
    val client = builder.build
    val res = HttpClient.newHttpClient.send(client, HttpResponse.BodyHandlers.ofString)
    val status = res.statusCode.toString
    println(s"GET ${req} ${status}")
    res.body
  }

  def postActivity(username: String, hostname: String, req: String, x: Any) = {
    val t = DateTimeFormatter.ofPattern("EEE, dd MMM yyyy HH:mm:ss z", Locale.ENGLISH)
      .withZone(ZoneId.of("GMT"))
      .format(Instant.now)
    val body = objectMapper.writeValueAsString(x)
    val digest = MessageDigest.getInstance("SHA-256")
    digest.update(body.getBytes)
    val s256 = Base64.getEncoder.encodeToString(digest.digest)
    val sig = Signature.getInstance("SHA256withRSA")
    sig.initSign(PRIVATE_KEY)
    sig.update(List(
      s"(request-target): post ${new URI(req).getPath}",
      s"host: ${new URI(req).getHost}",
      s"date: ${t}",
      s"digest: SHA-256=${s256}"
    ).mkString("\n").getBytes)
    val b64 = Base64.getEncoder.encodeToString(sig.sign)
    val headers = Map(
      "Date" -> t,
      "Digest" -> s"SHA-256=${s256}",
      "Signature" -> List(
        s"keyId=\"https://${hostname}/u/${username}#Key\"",
        "algorithm=\"rsa-sha256\"",
        "headers=\"(request-target) host date digest\"",
        s"signature=\"${b64}\""
      ).mkString(","),
      "Accept" -> "application/json",
      "Accept-Encoding" -> "gzip",
      "Cache-Control" -> "max-age=0",
      "Content-Type" -> "application/activity+json",
      "User-Agent" -> s"StrawberryFields-Play/2.0.0 (+https://${hostname}/)"
    )
    println(s"POST ${req} ${body}")
    val builder = HttpRequest.newBuilder
      .uri(new URI(req))
      .POST(HttpRequest.BodyPublishers.ofString(body))
    headers.foreach {
      case (k, v) => builder.header(k, v)
    }
    val client = builder.build
    HttpClient.newHttpClient.send(client, HttpResponse.BodyHandlers.ofString)
  }

  def acceptFollow(username: String, hostname: String, x: JsonNode, y: JsonNode) = {
    val aid = uuidv7
    val body = Map(
      "@context" -> "https://www.w3.org/ns/activitystreams",
      "id" -> s"https://${hostname}/u/${username}/s/${aid}",
      "type" -> "Accept",
      "actor" -> s"https://${hostname}/u/${username}",
      "object" -> y
    )
    postActivity(username, hostname, x.get("inbox").textValue, body)
  }

  def follow(username: String, hostname: String, x: JsonNode) = {
    val aid = uuidv7
    val body = Map(
      "@context" -> "https://www.w3.org/ns/activitystreams",
      "id" -> s"https://${hostname}/u/${username}/s/${aid}",
      "type" -> "Follow",
      "actor" -> s"https://${hostname}/u/${username}",
      "object" -> x.get("id").textValue
    )
    postActivity(username, hostname, x.get("inbox").textValue, body)
  }

  def undoFollow(username: String, hostname: String, x: JsonNode) = {
    val aid = uuidv7
    val body = Map(
      "@context" -> "https://www.w3.org/ns/activitystreams",
      "id" -> s"https://${hostname}/u/${username}/s/${aid}#Undo",
      "type" -> "Undo",
      "actor" -> s"https://${hostname}/u/${username}",
      "object" -> Map(
        "id" -> s"https://${hostname}/u/${username}/s/${aid}",
        "type" -> "Follow",
        "actor" -> s"https://${hostname}/u/${username}",
        "object" -> x.get("id").textValue
      )
    )
    postActivity(username, hostname, x.get("inbox").textValue, body)
  }

  def like(username: String, hostname: String, x: JsonNode, y: JsonNode) = {
    val aid = uuidv7
    val body = Map(
      "@context" -> "https://www.w3.org/ns/activitystreams",
      "id" -> s"https://${hostname}/u/${username}/s/${aid}",
      "type" -> "Like",
      "actor" -> s"https://${hostname}/u/${username}",
      "object" -> x.get("id").textValue
    )
    postActivity(username, hostname, y.get("inbox").textValue, body)
  }

  def undoLike(username: String, hostname: String, x: JsonNode, y: JsonNode) = {
    val aid = uuidv7
    val body = Map(
      "@context" -> "https://www.w3.org/ns/activitystreams",
      "id" -> s"https://${hostname}/u/${username}/s/${aid}#Undo",
      "type" -> "Undo",
      "actor" -> s"https://${hostname}/u/${username}",
      "object" -> Map(
        "id" -> s"https://${hostname}/u/${username}/s/${aid}",
        "type" -> "Like",
        "actor" -> s"https://${hostname}/u/${username}",
        "object" -> x.get("id").textValue
      )
    )
    postActivity(username, hostname, y.get("inbox").textValue, body)
  }

  def announce(username: String, hostname: String, x: JsonNode, y: JsonNode) = {
    val aid = uuidv7
    val t = Instant.now.truncatedTo(ChronoUnit.SECONDS).toString
    val body = Map(
      "@context" -> "https://www.w3.org/ns/activitystreams",
      "id" -> s"https://${hostname}/u/${username}/s/${aid}/activity",
      "type" -> "Announce",
      "actor" -> s"https://${hostname}/u/${username}",
      "published" -> t,
      "to" -> List("https://www.w3.org/ns/activitystreams#Public"),
      "cc" -> List(s"https://${hostname}/u/${username}/followers"),
      "object" -> x.get("id").textValue
    )
    postActivity(username, hostname, y.get("inbox").textValue, body)
  }

  def undoAnnounce(username: String, hostname: String, x: JsonNode, y: JsonNode, z: String) = {
    val aid = uuidv7
    val body = Map(
      "@context" -> "https://www.w3.org/ns/activitystreams",
      "id" -> s"https://${hostname}/u/${username}/s/${aid}#Undo",
      "type" -> "Undo",
      "actor" -> s"https://${hostname}/u/${username}",
      "object" -> Map(
        "id" -> s"${z}/activity",
        "type" -> "Announce",
        "actor" -> s"https://${hostname}/u/${username}",
        "to" -> List("https://www.w3.org/ns/activitystreams#Public"),
        "cc" -> List(s"https://${hostname}/u/${username}/followers"),
        "object" -> x.get("id").textValue
      )
    )
    postActivity(username, hostname, y.get("inbox").textValue, body)
  }

  def createNote(username: String, hostname: String, x: JsonNode, y: String) = {
    val aid = uuidv7
    val t = Instant.now.truncatedTo(ChronoUnit.SECONDS).toString
    val body = Map(
      "@context" -> "https://www.w3.org/ns/activitystreams",
      "id" -> s"https://${hostname}/u/${username}/s/${aid}/activity",
      "type" -> "Create",
      "actor" -> s"https://${hostname}/u/${username}",
      "published" -> t,
      "to" -> List("https://www.w3.org/ns/activitystreams#Public"),
      "cc" -> List(s"https://${hostname}/u/${username}/followers"),
      "object" -> Map(
        "id" -> s"https://${hostname}/u/${username}/s/${aid}",
        "type" -> "Note",
        "attributedTo" -> s"https://${hostname}/u/${username}",
        "content" -> talkScript(y),
        "url" -> s"https://${hostname}/u/${username}/s/${aid}",
        "published" -> t,
        "to" -> List("https://www.w3.org/ns/activitystreams#Public"),
        "cc" -> List(s"https://${hostname}/u/${username}/followers")
      )
    )
    postActivity(username, hostname, x.get("inbox").textValue, body)
  }

  def createNoteImage(username: String, hostname: String, x: JsonNode, y: String, z: String) = {
    val aid = uuidv7
    val t = Instant.now.truncatedTo(ChronoUnit.SECONDS).toString
    val body = Map(
      "@context" -> "https://www.w3.org/ns/activitystreams",
      "id" -> s"https://${hostname}/u/${username}/s/${aid}/activity",
      "type" -> "Create",
      "actor" -> s"https://${hostname}/u/${username}",
      "published" -> t,
      "to" -> List("https://www.w3.org/ns/activitystreams#Public"),
      "cc" -> List(s"https://${hostname}/u/${username}/followers"),
      "object" -> Map(
        "id" -> s"https://${hostname}/u/${username}/s/${aid}",
        "type" -> "Note",
        "attributedTo" -> s"https://${hostname}/u/${username}",
        "content" -> talkScript("https://localhost"),
        "url" -> s"https://${hostname}/u/${username}/s/${aid}",
        "published" -> t,
        "to" -> List("https://www.w3.org/ns/activitystreams#Public"),
        "cc" -> List(s"https://${hostname}/u/${username}/followers"),
        "attachment" -> List(
          Map(
            "type" -> "Image",
            "mediaType" -> z,
            "url" -> y
          )
        )
      )
    )
    postActivity(username, hostname, x.get("inbox").textValue, body)
  }

  def createNoteMention(username: String, hostname: String, x: JsonNode, y: JsonNode, z: String) = {
    val aid = uuidv7
    val t = Instant.now.truncatedTo(ChronoUnit.SECONDS).toString
    val at = s"@${y.get("preferredUsername").textValue}@${new URI(y.get("inbox").textValue).getHost}"
    val body = Map(
      "@context" -> "https://www.w3.org/ns/activitystreams",
      "id" -> s"https://${hostname}/u/${username}/s/${aid}/activity",
      "type" -> "Create",
      "actor" -> s"https://${hostname}/u/${username}",
      "published" -> t,
      "to" -> List("https://www.w3.org/ns/activitystreams#Public"),
      "cc" -> List(s"https://${hostname}/u/${username}/followers"),
      "object" -> Map(
        "id" -> s"https://${hostname}/u/${username}/s/${aid}",
        "type" -> "Note",
        "attributedTo" -> s"https://${hostname}/u/${username}",
        "inReplyTo" -> x.get("id").textValue,
        "content" -> talkScript(z),
        "url" -> s"https://${hostname}/u/${username}/s/${aid}",
        "published" -> t,
        "to" -> List("https://www.w3.org/ns/activitystreams#Public"),
        "cc" -> List(s"https://${hostname}/u/${username}/followers"),
        "tag" -> List(
          Map(
            "type" -> "Mention",
            "name" -> at
          )
        )
      )
    )
    postActivity(username, hostname, y.get("inbox").textValue, body)
  }

  def createNoteHashtag(username: String, hostname: String, x: JsonNode, y: String, z: String) = {
    val aid = uuidv7
    val t = Instant.now.truncatedTo(ChronoUnit.SECONDS).toString
    val body = Map(
      "@context" -> List(
        "https://www.w3.org/ns/activitystreams",
        Map("Hashtag" -> "as:Hashtag")
      ),
      "id" -> s"https://${hostname}/u/${username}/s/${aid}/activity",
      "type" -> "Create",
      "actor" -> s"https://${hostname}/u/${username}",
      "published" -> t,
      "to" -> List("https://www.w3.org/ns/activitystreams#Public"),
      "cc" -> List(s"https://${hostname}/u/${username}/followers"),
      "object" -> Map(
        "id" -> s"https://${hostname}/u/${username}/s/${aid}",
        "type" -> "Note",
        "attributedTo" -> s"https://${hostname}/u/${username}",
        "content" -> talkScript(y),
        "url" -> s"https://${hostname}/u/${username}/s/${aid}",
        "published" -> t,
        "to" -> List("https://www.w3.org/ns/activitystreams#Public"),
        "cc" -> List(s"https://${hostname}/u/${username}/followers"),
        "tag" -> List(
          Map(
            "type" -> "Hashtag",
            "name" -> s"#${z}"
          )
        )
      )
    )
    postActivity(username, hostname, x.get("inbox").textValue, body)
  }

  def updateNote(username: String, hostname: String, x: JsonNode, y: String) = {
    val t = Instant.now.truncatedTo(ChronoUnit.SECONDS).toString
    val ts = System.currentTimeMillis
    val body = Map(
      "@context" -> "https://www.w3.org/ns/activitystreams",
      "id" -> s"${y}#${ts}",
      "type" -> "Update",
      "actor" -> s"https://${hostname}/u/${username}",
      "published" -> t,
      "to" -> List("https://www.w3.org/ns/activitystreams#Public"),
      "cc" -> List(s"https://${hostname}/u/${username}/followers"),
      "object" -> Map(
        "id" -> y,
        "type" -> "Note",
        "attributedTo" -> s"https://${hostname}/u/${username}",
        "content" -> talkScript("https://localhost"),
        "url" -> y,
        "updated" -> t,
        "to" -> List("https://www.w3.org/ns/activitystreams#Public"),
        "cc" -> List(s"https://${hostname}/u/${username}/followers")
      )
    )
    postActivity(username, hostname, x.get("inbox").textValue, body)
  }

  def deleteTombstone(username: String, hostname: String, x: JsonNode, y: String) = {
    val body = Map(
      "@context" -> "https://www.w3.org/ns/activitystreams",
      "id" -> s"${y}#Delete",
      "type" -> "Delete",
      "actor" -> s"https://${hostname}/u/${username}",
      "object" -> Map(
        "id" -> y,
        "type" -> "Tombstone"
      )
    )
    postActivity(username, hostname, x.get("inbox").textValue, body)
  }

  /**
   * Create an Action to render an HTML page.
   *
   * The configuration in the `routes` file means that this method
   * will be called when the application receives a `GET` request with
   * a path of `/`.
   */
  def index() = Action { implicit request: Request[AnyContent] =>
    Ok("StrawberryFields Play")
  }

  def about() = Action { implicit request: Request[AnyContent] =>
    Ok("About: Blank")
  }

  def uUser(username: String) = Action { implicit request: Request[AnyContent] =>
    val hostname = new URI(CONFIG.get("origin").textValue).getHost
    val acceptHeaderField = request.headers.get("Accept").toString
    val body: JsValue = Json.obj(
      "@context" -> Json.arr(
        "https://www.w3.org/ns/activitystreams",
        "https://w3id.org/security/v1",
        Json.obj(
          "schema" -> "https://schema.org/",
          "PropertyValue" -> "schema:PropertyValue",
          "value" -> "schema:value",
          "Key" -> "sec:Key"
        )
      ),
      "id" -> s"https://${hostname}/u/${username}",
      "type" -> "Person",
      "inbox" -> s"https://${hostname}/u/${username}/inbox",
      "outbox" -> s"https://${hostname}/u/${username}/outbox",
      "following" -> s"https://${hostname}/u/${username}/following",
      "followers" -> s"https://${hostname}/u/${username}/followers",
      "preferredUsername" -> username,
      "name" -> CONFIG.get("actor").get(0).get("name").textValue,
      "summary" -> "<p>2.0.0</p>",
      "url" -> s"https://${hostname}/u/${username}",
      "endpoints" -> Json.obj("sharedInbox" -> s"https://${hostname}/u/${username}/inbox"),
      "attachment" -> Json.arr(
        Json.obj(
          "type" -> "PropertyValue",
          "name" -> "me",
          "value" -> ME
        )
      ),
      "icon" -> Json.obj(
        "type" -> "Image",
        "mediaType" -> "image/png",
        "url" -> s"https://${hostname}/public/${username}u.png"
      ),
      "image" -> Json.obj(
        "type" -> "Image",
        "mediaType" -> "image/png",
        "url" -> s"https://${hostname}/public/${username}s.png"
      ),
      "publicKey" -> Json.obj(
        "id" -> s"https://${hostname}/u/${username}#Key",
        "type" -> "Key",
        "owner" -> s"https://${hostname}/u/${username}",
        "publicKeyPem" -> publicKeyPem
      )
    )
    if (username != CONFIG.get("actor").get(0).get("preferredUsername").textValue) NotFound(Json.obj())
    else if (
      !(acceptHeaderField.contains("application/activity+json")
        || acceptHeaderField.contains("application/ld+json")
        || acceptHeaderField.contains("application/json"))
    ) Ok(s"${username}: ${CONFIG.get("actor").get(0).get("name").textValue}").withHeaders(
      "Cache-Control" -> s"public, max-age=${CONFIG.get("ttl").asText()}, must-revalidate",
      "Vary" -> "Accept, Accept-Encoding"
    ) else Ok(body).as("application/activity+json").withHeaders(
      "Cache-Control" -> s"public, max-age=${CONFIG.get("ttl").asText()}, must-revalidate",
      "Vary" -> "Accept, Accept-Encoding"
    )
  }

  def getInbox(username: String) = Action { implicit request: Request[AnyContent] =>
    MethodNotAllowed
  }
  def postInbox(username: String) = Action(parse.tolerantText) { implicit request: Request[String] =>
    val hostname = new URI(CONFIG.get("origin").textValue).getHost
    val contentTypeHeaderField = request.headers.get("Content-Type").toString
    val y = request.body
    val ny = objectMapper.readTree(y)
    var t = Option(ny.path("type").textValue).getOrElse("")
    val aid = Option(ny.path("id").textValue).getOrElse("")
    val atype = Option(ny.path("type").textValue).getOrElse("")
    if (aid.length > 1024 || atype.length > 64) BadRequest
    else {
      println(s"INBOX ${aid} ${atype}")
      if (username != CONFIG.get("actor").get(0).get("preferredUsername").textValue) NotFound(Json.obj())
      else if (
        !(contentTypeHeaderField.contains("application/activity+json")
          || contentTypeHeaderField.contains("application/ld+json")
          || contentTypeHeaderField.contains("application/json"))
        || request.headers.get("Digest") == null
        || request.headers.get("Digest").isEmpty
        || request.headers.get("Signature") == null
        || request.headers.get("Signature").isEmpty
      ) BadRequest
      else if (
        t == "Accept"
        || t == "Reject"
        || t == "Add"
        || t == "Remove"
        || t == "Like"
        || t == "Announce"
        || t == "Create"
        || t == "Update"
        || t == "Delete"
      ) Ok
      else if (t == "Follow") {
        if (new URI(Option(ny.path("actor").textValue).getOrElse("")).getScheme != "https") BadRequest
        else {
          val x = getActivity(username, hostname, ny.get("actor").textValue)
          if (x == null || x.isEmpty) InternalServerError
          else {
            val nx = objectMapper.readTree(x)
            acceptFollow(username, hostname, nx, ny)
            Ok
          }
        }
      } else if (t == "Undo") {
        val nz = ny.withObject("object").asInstanceOf[ObjectNode]
        t = Option(nz.path("type").textValue).getOrElse("")
        if (
          t == "Accept"
          || t == "Like"
          || t == "Announce"
        ) Ok
        else if (t == "Follow") {
          if (new URI(Option(ny.path("actor").textValue).getOrElse("")).getScheme != "https") BadRequest
          else {
            val x = getActivity(username, hostname, ny.get("actor").textValue)
            if (x == null || x.isEmpty) InternalServerError
            else {
              val nx = objectMapper.readTree(x)
              acceptFollow(username, hostname, nx, nz)
              Ok
            }
          }
        } else InternalServerError
      } else InternalServerError
    }
  }

  def postOutbox(username: String) = Action { implicit request: Request[AnyContent] =>
    MethodNotAllowed
  }
  def getOutbox(username: String) = Action { implicit request: Request[AnyContent] =>
    val hostname = new URI(CONFIG.get("origin").textValue).getHost
    val body: JsValue = Json.obj(
      "@context" -> "https://www.w3.org/ns/activitystreams",
      "id" -> s"https://${hostname}/u/${username}/outbox",
      "type" -> "OrderedCollection",
      "totalItems" -> 0
    )
    if (username != CONFIG.get("actor").get(0).get("preferredUsername").textValue) NotFound(Json.obj())
    else Ok(body).as("application/activity+json")
  }

  def following(username: String) = Action { implicit request: Request[AnyContent] =>
    val hostname = new URI(CONFIG.get("origin").textValue).getHost
    val body: JsValue = Json.obj(
      "@context" -> "https://www.w3.org/ns/activitystreams",
      "id" -> s"https://${hostname}/u/${username}/following",
      "type" -> "OrderedCollection",
      "totalItems" -> 0
    )
    if (username != CONFIG.get("actor").get(0).get("preferredUsername").textValue) NotFound(Json.obj())
    else Ok(body).as("application/activity+json")
  }

  def followers(username: String) = Action { implicit request: Request[AnyContent] =>
    val hostname = new URI(CONFIG.get("origin").textValue).getHost
    val body: JsValue = Json.obj(
      "@context" -> "https://www.w3.org/ns/activitystreams",
      "id" -> s"https://${hostname}/u/${username}/followers",
      "type" -> "OrderedCollection",
      "totalItems" -> 0
    )
    if (username != CONFIG.get("actor").get(0).get("preferredUsername").textValue) NotFound(Json.obj())
    else Ok(body).as("application/activity+json")
  }

  def sSend(secret: String, username: String) = Action(parse.tolerantText) { implicit request: Request[String] =>
    val hostname = new URI(CONFIG.get("origin").textValue).getHost
    val body = request.body
    val send = objectMapper.readTree(body)
    val t = Option(send.path("type").textValue).getOrElse("")
    if (
      username != CONFIG.get("actor").get(0).get("preferredUsername").textValue
      || secret == null
      || secret.isEmpty
      || secret == "-"
      || secret != dotenv.get("SECRET")
    ) NotFound(Json.obj())
    else if (new URI(Option(send.path("id").textValue).getOrElse("")).getScheme != "https") BadRequest
    else {
      val x = getActivity(username, hostname, send.get("id").textValue)
      if (x == null || x.isEmpty) InternalServerError
      else {
        val nx = objectMapper.readTree(x)
        val aid = Option(nx.path("id").textValue).getOrElse("")
        val atype = Option(nx.path("type").textValue).getOrElse("")
        if (t == "follow") {
            follow(username, hostname, nx)
            Ok
        } else if (t == "undo_follow") {
            undoFollow(username, hostname, nx)
            Ok
        } else if (t == "like") {
          if (new URI(Option(nx.path("attributedTo").textValue).getOrElse("")).getScheme != "https") BadRequest
          else {
            val y = getActivity(username, hostname, nx.get("attributedTo").textValue)
            if (y == null || y.isEmpty) InternalServerError
            else {
              val ny = objectMapper.readTree(y)
              like(username, hostname, nx, ny)
              Ok
            }
          }
        } else if (t == "undo_like") {
          if (new URI(Option(nx.path("attributedTo").textValue).getOrElse("")).getScheme != "https") BadRequest
          else {
            val y = getActivity(username, hostname, nx.get("attributedTo").textValue)
            if (y == null || y.isEmpty) InternalServerError
            else {
              val ny = objectMapper.readTree(y)
              undoLike(username, hostname, nx, ny)
              Ok
            }
          }
        } else if (t == "announce") {
          if (new URI(Option(nx.path("attributedTo").textValue).getOrElse("")).getScheme != "https") BadRequest
          else {
            val y = getActivity(username, hostname, nx.get("attributedTo").textValue)
            if (y == null || y.isEmpty) InternalServerError
            else {
              val ny = objectMapper.readTree(y)
              announce(username, hostname, nx, ny)
              Ok
            }
          }
        } else if (t == "undo_announce") {
          if (new URI(Option(nx.path("attributedTo").textValue).getOrElse("")).getScheme != "https") BadRequest
          else {
            val y = getActivity(username, hostname, nx.get("attributedTo").textValue)
            if (y == null || y.isEmpty) InternalServerError
            else {
              val ny = objectMapper.readTree(y)
              val z =
                Option(send.path("url").textValue)
                  .filter(_.nonEmpty)
                  .getOrElse(s"https://${hostname}/u/${username}/s/00000000000000000000000000000000")
              if (new URI(z).getScheme != "https") BadRequest
              else {
                undoAnnounce(username, hostname, nx, ny, z)
                Ok
              }
            }
          }
        } else if (t == "create_note") {
          val y = Option(send.path("url").textValue).filter(_.nonEmpty).getOrElse("https://localhost")
          if (new URI(y).getScheme != "https") BadRequest
          else {
            createNote(username, hostname, nx, y)
            Ok
          }
        } else if (t == "create_note_image") {
          val y =
            Option(send.path("url").textValue)
              .filter(_.nonEmpty)
              .getOrElse(s"https://${hostname}/public/logo.png")
          if (new URI(y).getScheme != "https" || new URI(y).getHost != hostname) BadRequest
          else {
            val z =
              if (y.endsWith(".jpg") || y.endsWith(".jpeg")) "image/jpeg"
              else if (y.endsWith(".svg")) "image/svg+xml"
              else if (y.endsWith(".gif")) "image/gif"
              else if (y.endsWith(".webp")) "image/webp"
              else if (y.endsWith(".avif")) "image/avif"
              else "image/png"
            createNoteImage(username, hostname, nx, y, z)
            Ok
          }
        } else if (t == "create_note_mention") {
          if (new URI(Option(nx.path("attributedTo").textValue).getOrElse("")).getScheme != "https") BadRequest
          else {
            val y = getActivity(username, hostname, nx.get("attributedTo").textValue)
            if (y == null || y.isEmpty) InternalServerError
            else {
              val ny = objectMapper.readTree(y)
              val z = Option(send.path("url").textValue).filter(_.nonEmpty).getOrElse("https://localhost")
              if (new URI(z).getScheme != "https") BadRequest
              else {
                createNoteMention(username, hostname, nx, ny, z)
                Ok
              }
            }
          }
        } else if (t == "create_note_hashtag") {
          val y = Option(send.path("url").textValue).filter(_.nonEmpty).getOrElse("https://localhost")
          if (new URI(y).getScheme != "https") BadRequest
          else {
            val z = Option(send.path("tag").textValue).filter(_.nonEmpty).getOrElse("Hashtag")
            createNoteHashtag(username, hostname, nx, y, z)
            Ok
          }
        } else if (t == "update_note") {
          val y =
            Option(send.path("url").textValue)
              .filter(_.nonEmpty)
              .getOrElse(s"https://${hostname}/u/${username}/s/00000000000000000000000000000000")
          if (new URI(y).getScheme != "https") BadRequest
          else {
            updateNote(username, hostname, nx, y)
            Ok
          }
        } else if (t == "delete_tombstone") {
          val y =
            Option(send.path("url").textValue)
              .filter(_.nonEmpty)
              .getOrElse(s"https://${hostname}/u/${username}/s/00000000000000000000000000000000")
          if (new URI(y).getScheme != "https") BadRequest
          else {
            deleteTombstone(username, hostname, nx, y)
            Ok
          }
        } else {
          println(s"TYPE ${aid} ${atype}")
          Ok
        }
      }
    }
  }

  def nodeinfo() = Action { implicit request: Request[AnyContent] =>
    val hostname = new URI(CONFIG.get("origin").textValue).getHost
    val body: JsValue = Json.obj(
      "links" -> Json.arr(
        Json.obj(
          "rel" -> "http://nodeinfo.diaspora.software/ns/schema/2.0",
          "href" -> s"https://${hostname}/nodeinfo/2.0.json"
        ),
        Json.obj(
          "rel" -> "http://nodeinfo.diaspora.software/ns/schema/2.1",
          "href" -> s"https://${hostname}/nodeinfo/2.1.json"
        )
      )
    )
    Ok(body).withHeaders(
      "Cache-Control" -> s"public, max-age=${CONFIG.get("ttl").asText()}, must-revalidate",
      "Vary" -> "Accept, Accept-Encoding"
    )
  }

  def webfinger() = Action { implicit request: Request[AnyContent] =>
    val username = CONFIG.get("actor").get(0).get("preferredUsername").textValue
    val hostname = new URI(CONFIG.get("origin").textValue).getHost
    var p443 = s"https://${hostname}:443/"
    var resource = request.getQueryString("resource").getOrElse("")
    val body: JsValue = Json.obj(
      "subject" -> s"acct:${username}@${hostname}",
      "aliases" -> Json.arr(
        s"mailto:${username}@${hostname}",
        s"https://${hostname}/@${username}",
        s"https://${hostname}/u/${username}",
        s"https://${hostname}/user/${username}",
        s"https://${hostname}/users/${username}"
      ),
      "links" -> Json.arr(
        Json.obj(
          "rel" -> "self",
          "type" -> "application/activity+json",
          "href" -> s"https://${hostname}/u/${username}"
        ),
        Json.obj(
          "rel" -> "http://webfinger.net/rel/avatar",
          "type" -> "image/png",
          "href" -> s"https://${hostname}/public/${username}u.png"
        ),
        Json.obj(
          "rel" -> "http://webfinger.net/rel/profile-page",
          "type" -> "text/plain",
          "href" -> s"https://${hostname}/u/${username}"
        )
      )
    )
    if (resource.startsWith(p443)) resource = s"https://${hostname}/${resource.substring(p443.length)}"
    if (
      !(resource == s"acct:${username}@${hostname}"
        || resource == s"mailto:${username}@${hostname}"
        || resource == s"https://${hostname}/@${username}"
        || resource == s"https://${hostname}/u/${username}"
        || resource == s"https://${hostname}/user/${username}"
        || resource == s"https://${hostname}/users/${username}")
    ) NotFound(Json.obj())
    else Ok(body).as("application/jrd+json").withHeaders(
      "Cache-Control" -> s"public, max-age=${CONFIG.get("ttl").asText()}, must-revalidate",
      "Vary" -> "Accept, Accept-Encoding"
    )
  }

  def u() = Action { implicit request: Request[AnyContent] =>
    Redirect(routes.HomeController.index(), FOUND)
  }

  def uu(username: String) = Action { implicit request: Request[AnyContent] =>
    Redirect(routes.HomeController.uUser(username), FOUND)
  }
}
