name := """strawberryfields-play"""
organization := "io.gitlab.acefed"

version := "2.0.0"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.13.15"

libraryDependencies += guice
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "7.0.1" % Test

// Adds additional packages into Twirl
// TwirlKeys.templateImports += "io.gitlab.acefed.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "io.gitlab.acefed.binders._"

libraryDependencies += "com.fasterxml.jackson.core" % "jackson-databind" % "2.18.0"
libraryDependencies += "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.18.0"
libraryDependencies += "io.github.cdimascio" % "dotenv-java" % "3.0.2"
