# StrawberryFields Play

- [2.0.0]
- [1.6.0] - 2024-08-10 - 9 files changed, 147 insertions(+), 106 deletions(-)
- [1.5.0] - 2024-03-10 - 3 files changed, 136 insertions(+), 67 deletions(-)
- [1.4.0] - 2024-02-04 - 7 files changed, 109 insertions(+), 63 deletions(-)
- [1.3.0] - 2023-11-11 - 7 files changed, 34 insertions(+), 24 deletions(-)
- [1.2.0] - 2023-11-05 - 11 files changed, 373 insertions(+), 241 deletions(-)
- [1.1.0] - 2023-08-19 - 5 files changed, 13 insertions(+), 14 deletions(-)
- 1.0.0 - 2023-08-16

[2.0.0]: https://gitlab.com/acefed/strawberryfields-play/-/compare/7cf849f2...main
[1.6.0]: https://gitlab.com/acefed/strawberryfields-play/-/compare/9ee48ede...7cf849f2
[1.5.0]: https://gitlab.com/acefed/strawberryfields-play/-/compare/0c254204...9ee48ede
[1.4.0]: https://gitlab.com/acefed/strawberryfields-play/-/compare/ce007cb5...0c254204
[1.3.0]: https://gitlab.com/acefed/strawberryfields-play/-/compare/77ad54af...ce007cb5
[1.2.0]: https://gitlab.com/acefed/strawberryfields-play/-/compare/12ba8236...77ad54af
[1.1.0]: https://gitlab.com/acefed/strawberryfields-play/-/compare/2f10f6c4...12ba8236
